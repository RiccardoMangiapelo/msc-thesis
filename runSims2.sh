SECONDS=0
source ~/.bashrc
kinit && aklog

echo "######### Starting ######### "
echo "1/11"
python Sim.py -i parameters/Rparameters.config

echo "2/11"
python Sim.py -i parameters/Rparameters2.config

echo "3/11"
python Sim.py -i parameters/Rparameters3.config

echo "4/11"
python Sim.py -i parameters/Rparameters4.config

echo "5/11"
python Sim.py -i parameters/Rparameters5.config

echo "6/11"
python Sim.py -i parameters/Rparameters6.config

echo "7/11"
python Sim.py -i parameters/Tparameters.config

echo "8/11"
python Sim.py -i parameters/Tparameters2.config

echo "9/11"
python Sim.py -i parameters/Tparameters3.config

echo "10/11"
python Sim.py -i parameters/Tparameters4.config

echo "11/11"
python Sim.py -i parameters/Tparameters5.config

echo "######### Finished #########"

if (( $SECONDS > 3600 )) ; then
    let "hours=SECONDS/3600"
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "Completed in $hours hour(s), $minutes minute(s) and $seconds second(s)"
elif (( $SECONDS > 60 )) ; then
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "Completed in $minutes minute(s) and $seconds second(s)"
else
    echo "Completed in $SECONDS seconds"
fi
