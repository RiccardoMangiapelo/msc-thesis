def power2untilN(n):
    res = 0
    i = 1
    prevI = i
    while res < n:
        prevI = res
        res = 2**i
        i+=1
    return prevI

# print("Final answer: ",power2untilN(50))

################################################################################
################################################################################

# find duplicates THIS IS LINEAR
def findDuplictes(mylist):
    set_list = set(mylist)
    if len(set_list) == len(mylist):
        return False
    else:
        return True

mylist = ["A","b","sd"]
print(findDuplictes(mylist))



################################################################################
################################################################################

# ONLY INTEGERS
# find the element that doesnt appear twice in a list
def singleNumber(self, nums):
    a = 0
    for i in nums:
        a ^= i
    return a

# GENERAL
# find the element that doesnt appear twice in a list
def singleNumber(self, nums):
    hash_table = {}
    for i in nums:
        try:
            hash_table.pop(i)
        except:
            hash_table[i] = 1
    return hash_table.popitem()[0]



################################################################################
################################################################################


# find happy number
def happy(n):
    hm = {}
    while n != 1:
        if n in hm:
            return False
        hm[n] = True
        stringify = str(n)
        res = 0
        for d in stringify:
            res += int(d) ** 2
        n = res
    return True

print(happy(19))