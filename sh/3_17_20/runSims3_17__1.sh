SECONDS=0
source ~/.bashrc
kinit && aklog

echo "######### Starting ######### "
echo "1/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters.config

echo "2/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters2.config

echo "3/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters3.config

echo "4/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters4.config

echo "5/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters5.config

echo "6/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters6.config

echo "7/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters7.config

echo "8/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters8.config

echo "9/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters9.config

echo "10/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters10.config

echo "11/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters11.config

echo "12/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters12.config

echo "13/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters13.config

echo "14/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters14.config

echo "15/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters15.config

echo "16/16"
python Sim.py -i parameters/3_17_20/exp_1/parameters16.config

echo "######### Finished #########"

if (( $SECONDS > 3600 )) ; then
    let "hours=SECONDS/3600"
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "Completed in $hours hour(s), $minutes minute(s) and $seconds second(s)"
elif (( $SECONDS > 60 )) ; then
    let "minutes=(SECONDS%3600)/60"
    let "seconds=(SECONDS%3600)%60"
    echo "Completed in $minutes minute(s) and $seconds second(s)"
else
    echo "Completed in $SECONDS seconds"
fi
