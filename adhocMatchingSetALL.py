import numpy as np
import Create_Agents
import random
import config
import Agent
import matplotlib.pyplot as plt
import Plot
import time
import ExpectedValue
import Plot_Histograms
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import dill
import glob
import os
from collections import defaultdict

cd = os.getcwd()
path = "./../all_results/matchingSet/"
os.chdir(path)
left = "Qlearning"
right = "Epsilon Threshold"
# left = "Qlearning Exp"
# right = left
pickleFile = "qlearning_eps_99.pkl"

try:
    with open(pickleFile, 'rb') as f:
        agents_RIGHT, agents_LEFT, playerinfo, playerlearning, BOT_avg_expV, MID_avg_expV, TOP_avg_expV, error_bar, expUtility_LEFT, expUtility_RIGHT, allVals = dill.load(f)
except:
    with open(pickleFile, 'rb') as f:
        agents_RIGHT, agents_LEFT, playerinfo, playerlearning, ALLavg_expV, error_bar, expUtility_LEFT, expUtility_RIGHT, allVals = dill.load(f)

# n = 0
expUtility_LEFT = {}
expUtility_RIGHT = {}
info_dict_LEFT = {}
info_dict_RIGHT = {}
tot_agents_LEFT = len(agents_LEFT)
tot_agents_RIGHT = len(agents_RIGHT)
match_LEFT = defaultdict(lambda: [])
match_RIGHT = defaultdict(lambda: [])

for side in ['left', 'right']:
    if side == 'left':
        loop_through = agents_LEFT
        other_side = agents_RIGHT
    else:
        loop_through = agents_RIGHT
        other_side = agents_LEFT
    for agent in loop_through:
        # create a dictionary with the relevant information for each agent
        if agent.algo_type == 'Random':
            final_threshold = agent.threshold
        elif agent.algo_type == 'Theoretical':
            final_threshold = agent.threshold.item()
        else:
            final_threshold = agent.threshold_history[-1]
        # n_matched = sum(n+1 for x in agent.meeting_history if (x[2] == 'yes') and (x[3] == 'yes'))
        agent_type = agent.get_type()[0]
        gamma = agent.gamma
        if side == 'left':
            if agent.algo_type == "Model" or agent.algo_type == 'Theoretical' or agent.algo_type == 'Random':
                info_dict_LEFT.setdefault(agent_type, []).append((final_threshold, gamma))
            elif agent.algo_type == "Qlearn2" or agent.algo_type == "Qlearn":
                if final_threshold[1] == False:
                    poss_match = []
                    for i in other_side:
                        if agent.wouldmatch(i.get_type()):
                            poss_match.append(i.get_type()[0])
                    info_dict_LEFT.setdefault(agent_type, []).append((poss_match, gamma))
                else:
                    info_dict_LEFT.setdefault(agent_type, []).append((final_threshold[1], gamma))
        else:
            if agent.algo_type == "Model" or agent.algo_type == 'Theoretical' or agent.algo_type == 'Random':
                info_dict_RIGHT.setdefault(agent_type, []).append((final_threshold, gamma))
            elif agent.algo_type == "Qlearn2" or agent.algo_type == "Qlearn":
                if final_threshold[1] == False:
                    poss_match = []
                    for i in other_side:
                        if agent.wouldmatch(i.get_type()):
                            poss_match.append(i.get_type()[0])
                    info_dict_RIGHT.setdefault(agent_type, []).append((poss_match, gamma))
                else:
                    info_dict_RIGHT.setdefault(agent_type, []).append((final_threshold[1], gamma))

for l_key, l_val in info_dict_LEFT.items():
    for r_key, r_val in info_dict_RIGHT.items():
        thisAgent_matches = []
        if type(r_val[0][0]) == int or type(r_val[0][0]) == float:
            if type(l_val[0][0]) == int or type(l_val[0][0]) == float:
                # if left agent type >= right agent threshold AND right agent type >= left agent threshold
                if l_key >= round(r_val[0][0]) and r_key >= round(l_val[0][0]):
                    match_LEFT.setdefault(l_key, thisAgent_matches).append(r_key)
            elif type(l_val[0][0] == list):
                if l_key >= round(r_val[0][0]) and r_key in l_val[0][0]:
                    match_LEFT.setdefault(l_key, thisAgent_matches).append(r_key)
        elif type(r_val[0][0]) == list:
            if type(l_val[0][0]) == int or type(l_val[0][0]) == float:
                if l_key in r_val[0][0] and r_key >= round(l_val[0][0]):
                    match_LEFT.setdefault(l_key, thisAgent_matches).append(r_key)
            elif type(l_val[0][0]) == list:
                if l_key in r_val[0][0] and r_key in l_val[0][0]:
                    match_LEFT.setdefault(l_key, thisAgent_matches).append(r_key)

for l_key, l_val in info_dict_RIGHT.items():
    for r_key, r_val in info_dict_LEFT.items():
        thisAgent_matches = []
        if type(r_val[0][0]) == int or type(r_val[0][0]) == float:
            if type(l_val[0][0]) == int or type(l_val[0][0]) == float:
                # if left agent type >= right agent threshold AND right agent type >= left agent threshold
                if l_key >= round(r_val[0][0]) and r_key >= round(l_val[0][0]):
                    match_RIGHT.setdefault(l_key, thisAgent_matches).append(r_key)
            elif type(l_val[0][0] == list):
                if l_key >= round(r_val[0][0]) and r_key in l_val[0][0]:
                    match_RIGHT.setdefault(l_key, thisAgent_matches).append(r_key)
        elif type(r_val[0][0]) == list:
            if type(l_val[0][0]) == int or type(l_val[0][0]) == float:
                if l_key in r_val[0][0] and r_key >= round(l_val[0][0]):
                    match_RIGHT.setdefault(l_key, thisAgent_matches).append(r_key)
            elif type(l_val[0][0]) == list:
                if l_key in r_val[0][0] and r_key in l_val[0][0]:
                    match_RIGHT.setdefault(l_key, thisAgent_matches).append(r_key)



fig, ax= plt.subplots()
# print(match_LEFT)
tup = sorted(match_RIGHT.items())
# x = list(range(1, 21))
x = [t[0] for t in tup]
y = [t[1] for t in tup]
for i,sublist in enumerate(y):
    for element in sublist:
        ax.plot(x[i], element, 'o', c='b')
ax.set_xlabel("Right Agent Type", fontsize=15)
ax.set_ylabel("Left Agent Type", fontsize=15)
ax.set_title(left+" vs "+right+ r'   $\gamma$:'+str(gamma), fontsize=19)
ax.set_xticks(np.arange(0, 21, 1.0))
ax.set_yticks(np.arange(0, 21, 1.0))

fig.savefig("Matching_"+left+"_"+right+"_"+str(gamma)+".pdf")
plt.close(fig)