from collections import defaultdict

def shortest_path_BFS(start, target, Adj):
    level = {start:0}
    parent = {start:None}
    i = 1
    frontier = [start]
    while frontier:
        next = []
        for f in frontier:
            for v in Adj[f]:
                if v not in level:
                    level[v]=i
                    parent[v]=f
                    next.append(v)
        frontier = next
        i+=1
    path = [target]
    node = target
    while node!=start:
        node = parent[node]
        path.append(node)
    print("One shortest path to get to ", target, " is ", list(reversed(path)), " which is reachable in ", level[target], " steps")


Adj = {'s':['a','d'] , 'a': ['b','c'], 'c':['f'], 'd':['e'], 'e':['v'], 'f':['v']}
Adj_default = defaultdict(lambda:[],Adj)
shortest_path_BFS('s','v', Adj_default )